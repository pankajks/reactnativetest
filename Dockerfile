# build buck from source
FROM ubuntu:18.04

RUN apt update  && apt install  -y --no-install-recommends ant git openjdk-8-jdk-headless curl unzip

# set default build arguments
ENV SDK_VERSION=commandlinetools-linux-7583922_latest.zip
# ENV SDK_VERSION=7583922
ENV ANDROID_BUILD_VERSION=29
ENV ANDROID_TOOLS_VERSION=29.0.3
ENV NDK_VERSION=20.1.5948944
ENV NODE_VERSION=14.x
ENV WATCHMAN_VERSION=4.9.0

# set default environment variables, please don't remove old env for compatibilty issue
ENV ADB_INSTALL_TIMEOUT=10
ENV ANDROID_HOME=/opt/android
ENV ANDROID_SDK_ROOT=${ANDROID_HOME}
ENV ANDROID_NDK=${ANDROID_HOME}/ndk/$NDK_VERSION
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

ENV PATH=${ANDROID_NDK}:${ANDROID_HOME}/cmdline-tools/latest/bin:${ANDROID_HOME}/emulator:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:/opt/buck/bin/:${PATH}


# Full reference at https://dl.google.com/android/repository/repository2-1.xml
# download and unpack android
# workaround buck clang version detection by symlinking
RUN curl -sS https://dl.google.com/android/repository/${SDK_VERSION} -o /tmp/sdk.zip \
    && mkdir -p ${ANDROID_HOME}/cmdline-tools \
    && unzip -q -d ${ANDROID_HOME}/cmdline-tools /tmp/sdk.zip \
    && mv ${ANDROID_HOME}/cmdline-tools/cmdline-tools ${ANDROID_HOME}/cmdline-tools/latest \
    && rm /tmp/sdk.zip \
    && yes | sdkmanager --licenses \
    && yes | sdkmanager "platform-tools" \
    && rm -rf ${ANDROID_HOME}/.android 

COPY package.json $HOME/app/
WORKDIR $HOME/app/
COPY . $HOME/app/
RUN export JAVA_HOME
# install nodejs and npm packages from nodesource
RUN  curl -sL https://deb.nodesource.com/setup_14.x | bash \
      && apt-get install -y nodejs \
      && rm -rf node_modules package-lock.json \
      && npm install \
      && npm install -g react-native-cli \ 
    #   && sh hack_to_browser.sh \
    #   && sh dev_env.sh \
      # && react-native link \
      && chmod +x android/gradlew
      
RUN  cd android \
     && ./gradlew assembleRelease --no-daemon
